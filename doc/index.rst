.. essai documentation master file, created by
   sphinx-quickstart on Tue Mar  7 09:45:29 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to essai's documentation!
=================================

.. image:: logo.png


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   toto


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
